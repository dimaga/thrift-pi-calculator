package com.test.implementations;

import com.test.gen.Task;
import com.test.gen.TaskResult;
import com.test.gen.WorkerService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

public class PiCalculatorClient {

    private WorkerService.Client client;
    private TTransport transport;

    public PiCalculatorClient(String host, int port) {
        initClient(host, port);
    }

    private void initClient(String host, int port) {
        try {
            transport = new TSocket(host, port);
            transport.open();

            TProtocol protocol = new TBinaryProtocol(transport);
            this.client = new WorkerService.Client(protocol);

        } catch (TException x) {
            x.printStackTrace();
        }
    }

    public void submitTask(String id, long startIndex, long endIndex) throws TException
    {
        this.client.submitTask(new Task(id, startIndex, endIndex));
    }

    public TaskResult getTaskResult(String id) throws TException {
        return this.client.getTaskResult(id);
    }
}
