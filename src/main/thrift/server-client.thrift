namespace java com.test.gen

typedef i32 int
typedef i64 long

service WorkerService
{
        void submitTask(1: Task task);
        TaskResult getTaskResult(1: string taskId);
}

struct Task {
    1: string taskId;
    2: long startIter;
    3: long endIter;
}

struct TaskResult {
    1: TaskStatus status;
    2: optional string result;
}

enum TaskStatus {
    RUNNING,
    DONE,
    NOT_FOUND
}