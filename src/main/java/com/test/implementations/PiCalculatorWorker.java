package com.test.implementations;

import com.test.gen.WorkerService;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

public class PiCalculatorWorker {
    public static WorkerService.Processor processor;
    public static WorkerServiceImpl workerService;

    public void startWorker(final int port) {
        workerService = new WorkerServiceImpl();
        processor = new WorkerService.Processor(workerService);

        Runnable runnable = () -> process(processor, port);

        new Thread(runnable).start();
    }

    private void process(WorkerService.Processor processor, int port) {
        try {
            TServerTransport serverTransport = new TServerSocket(port);
            TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));

            System.out.println("Starting pi calculator worker on port: " + port);
            server.serve();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
