This is implementation of distributed PI calculator with thrift and rpc. You can run program in two modes,
as worker or as client. To do so you need to pass some arguments when start program. First arg should be type
'client' or 'worker'.
If type is 'worker' than second argument should be port. Only two arguments required.
Example: 'worker 9091'
If type is 'client' second arg should be total number of iterations. And then host and port of workers. At least
one worker required.
Example: 'client 10000 localhost 9091 [localhost 9092 localhost 9093]'

Happy PI calculation :)