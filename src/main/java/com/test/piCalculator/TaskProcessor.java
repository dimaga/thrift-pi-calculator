package com.test.piCalculator;

import com.test.gen.Task;
import com.test.gen.TaskResult;
import com.test.gen.TaskStatus;
import com.test.piCalculator.PiCalculator;

import java.util.concurrent.Callable;

public class TaskProcessor implements Callable<TaskResult> {

    private final Task task;

    public TaskProcessor (Task task) {
        this.task = task;
    }

    public TaskResult call() throws Exception {
        System.out.println("Executing task: " + task.getTaskId());
        PiCalculator calculator = new PiCalculator();
        String calculationResult = calculator.calculatePi(task.getStartIter(), task.getEndIter()).toPlainString();
        TaskResult taskResult = new TaskResult(TaskStatus.DONE);
        taskResult.setResult(calculationResult);
        System.out.println("Finishing executing task: " + task.getTaskId());
        return taskResult;
    }
}