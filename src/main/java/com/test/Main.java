package com.test;

import com.test.implementations.PiCalculatorClient;
import com.test.implementations.PiCalculatorWorker;
import com.test.piCalculator.PiCalculatorService;
import org.apache.thrift.TException;

import java.util.*;

public class Main {
    private static final String HELP_TEXT =
            "ThriftDistributedPiCalculator is used to calculate PI number in distributed manner.\n" +
            "Application can be started as a worker, or as a client.\n" +
            "To start worker: first argument: 'worker', second argument: port.\n" +
            "To start client: first argument: 'client', second argument: total number of iterations, to calculate PI number, \n" +
            "worker_host, worker_port, [worker_host, worker_port] \n" +
            "\n" +
            "Examples: \n" +
            "'worker 9091'\n" +
            "'client 1000 localhost 9091 localhost 9092'\n" +
            "\n" +
            "Thank you for calculating PI number. It's very important for us. :)";

    public static void main (String... args) throws TException {
        if(args.length < 2) {
            System.out.println(HELP_TEXT);
        }

        if(args[0].equalsIgnoreCase("worker")) {
            //Worker here
            PiCalculatorWorker worker = new PiCalculatorWorker();
            worker.startWorker(Integer.valueOf(args[1]));
        } else if (args[0].equalsIgnoreCase("client")) {
            //Client here
            if (args.length < 4) {
                System.out.println("For client at least 4 arguments required: 'client', number of iteration, worker host, worker port.");
            }
            List<PiCalculatorClient> clients = new ArrayList<PiCalculatorClient>();
            long numOfIterations = Long.valueOf(args[1]);
            for (int i = 2; i < args.length; i+=2) {
                PiCalculatorClient client = new PiCalculatorClient(args[i], Integer.valueOf(args[i+1]));
                clients.add(client);
            }

            PiCalculatorService piCalculatorService = new PiCalculatorService(clients, numOfIterations);
            piCalculatorService.submitTasksToWorkers();
            piCalculatorService.fetchResults();
        } else {
            System.out.println(HELP_TEXT);
        }
    }
}
