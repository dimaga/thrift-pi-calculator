package com.test.implementations;

import com.test.piCalculator.TaskProcessor;
import com.test.gen.Task;
import com.test.gen.TaskResult;
import com.test.gen.TaskStatus;
import com.test.gen.WorkerService;
import org.apache.thrift.TException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

import static com.test.gen.TaskStatus.DONE;
import static com.test.gen.TaskStatus.RUNNING;

public class WorkerServiceImpl implements WorkerService.Iface {

    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Map<String, Future<TaskResult>> runningTasks = new HashMap<>();

    public void submitTask(Task task) throws TException {
        Future<TaskResult> taskResultFuture = executorService.submit(new TaskProcessor(task));
        runningTasks.put(task.getTaskId(), taskResultFuture);

        System.out.println("Submitted task with id " + task.getTaskId());
    }

    public TaskResult getTaskResult(String taskId) throws TException {
        TaskResult taskResult = new TaskResult();
        if(!runningTasks.containsKey(taskId)) {
            taskResult.setStatus(TaskStatus.NOT_FOUND);
            return taskResult;
        }

        if(runningTasks.get(taskId).isDone()) {
            taskResult.setStatus(DONE);
            try {
                taskResult = runningTasks.get(taskId).get();
                runningTasks.remove(taskId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            taskResult.setStatus(RUNNING);
        }
        System.out.println("Requested task with id " + taskId);
        System.out.println("Returning result with status '" + taskResult.getStatus() + "'");
        return taskResult;
    }
}
