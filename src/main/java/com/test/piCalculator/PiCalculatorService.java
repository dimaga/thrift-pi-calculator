package com.test.piCalculator;

import com.test.gen.TaskResult;
import com.test.gen.TaskStatus;
import com.test.implementations.PiCalculatorClient;
import org.apache.thrift.TException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PiCalculatorService {

    private List<PiCalculatorClient> clients;
    private long numOfIterations;
    final Map<String, PiCalculatorClient> taskToClientMapping = new HashMap<>();
    final Map<String, BigDecimal> calculatedResults = new HashMap<>();

    public PiCalculatorService(List<PiCalculatorClient> clients, long numOfIterations) {
        if (clients == null || clients.size() == 0) {
            throw new IllegalArgumentException("At least one PiCalculatorClient required.");
        }

        if(numOfIterations <= 0) {
            throw new IllegalArgumentException("Number of iterations must be positive.");
        }

        this.clients = clients;
        this.numOfIterations = numOfIterations;
    }


    public void submitTasksToWorkers() throws TException {
        int numOfPartitions = clients.size();
        long step = numOfIterations / numOfPartitions;

        for (int i = 0; i < numOfPartitions; i++) {
            long startIteration = i * step;
            long endIteration = (i + 1) * step;

            if (i == numOfPartitions - 1) {
                endIteration = numOfIterations;
            }

            String taskId = UUID.randomUUID().toString();
            clients.get(i).submitTask(taskId, startIteration, endIteration);
            System.out.println("Submit task: " + startIteration + "  " + endIteration);
            taskToClientMapping.put(taskId, clients.get(i));
        }
    }

    public void fetchResults() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        executorService.scheduleAtFixedRate(() -> {
            if (taskToClientMapping.size() == 0) {
                System.out.println("No running tasks. Calculating results.");
                BigDecimal sum = new BigDecimal(0);
                for (BigDecimal partialResult : calculatedResults.values()) {
                    sum = sum.add(partialResult);
                }

                calculatedResults.clear();
                System.out.println("Total sum is: " + sum);
                executorService.shutdown();

            } else {
                for (Map.Entry<String, PiCalculatorClient> calculatorClientEntry : taskToClientMapping.entrySet()) {
                    TaskResult taskResult = null;
                    try {
                        taskResult = calculatorClientEntry.getValue().getTaskResult(calculatorClientEntry.getKey());
                    } catch (TException e) {
                        e.printStackTrace();
                    }
                    if (taskResult.getStatus().equals(TaskStatus.DONE)) {
                        System.out.println("Received result for task with id: " + calculatorClientEntry.getKey());
                        calculatedResults.put(calculatorClientEntry.getKey(), new BigDecimal(taskResult.getResult()));
                    }
                }

                calculatedResults.forEach((key, value) -> taskToClientMapping.remove(key));

            }
        }, 0, 3, TimeUnit.SECONDS);

    }

}
